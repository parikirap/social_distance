package com.social.distance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.config.EnableMongoAuditing;

@SpringBootApplication
@EnableMongoAuditing
public class SocialDistance {

	public static void main(String[] args) {
		SpringApplication.run(SocialDistance.class, args);
	}

}
