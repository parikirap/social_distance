package com.social.distance.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.social.distance.model.User;
import com.social.distance.model.UserError;
import com.social.distance.model.response.Response;
import com.social.distance.model.response.UserResponse;
import com.social.distance.repository.UserDataRepository;

@Service
public class UserDistanceService {

	@Autowired
	private UserDataRepository userDataRepository;

	public ResponseEntity<Response<List<UserResponse>>> findPeopleAtDistance(String userId, int distance) {
		List<User> userList = userDataRepository.findAll();
		List<UserResponse> userAtDistance = new ArrayList<UserResponse>();
		Map<String, User> userMap = userList.stream().collect(Collectors.toMap(User::getUserId, user -> user));
		if (!userMap.containsKey(userId))
			return ResponseEntity.status(404)
					.body(new Response<List<UserResponse>>(false, UserError.USERID_NOT_FOUND.name(), null));

		LinkedList<String> queue = new LinkedList<String>();
		User user = userMap.get(userId);
		Map<String, Boolean> visited = new HashMap<String, Boolean>();
		visited.put(userId, true);
		for (String friend : user.getFriendList()) {
			visited.put(friend, true);
			queue.add(friend);
		}
		distance--;
		while (!queue.isEmpty() && distance > 0) {
			distance--;
			int size = queue.size();
			for (int j = 0; j < size; j++) {
				String front = queue.poll();
				List<String> adjacency = userMap.get(front).getFriendList();
				for (String node : adjacency) {
					if (visited.containsKey(node))
						continue;
					queue.add(node);
					visited.put(node, true);
				}
			}
		}

		while (!queue.isEmpty()) {
			user = userMap.get(queue.poll());
			userAtDistance.add(new UserResponse(user.getUserId(), user.getName(), user.getBio(), user.getFriendList()));
		}
		return ResponseEntity.ok()
				.body(new Response<List<UserResponse>>(true, UserError.SUCCESS.name(), userAtDistance));
	}
}
