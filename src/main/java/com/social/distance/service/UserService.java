package com.social.distance.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.social.distance.model.User;
import com.social.distance.model.UserError;
import com.social.distance.model.request.CreateUserDTO;
import com.social.distance.model.response.Response;
import com.social.distance.model.response.UserResponse;
import com.social.distance.repository.UserDataRepository;

@Service
public class UserService {

	@Autowired
	private UserDataRepository userDataRepository;

	private User checkAuth(String userId) {
		Optional<User> userOptional = userDataRepository.findById(userId);
		User user = userOptional.orElse(null);
		return user;
	}

	public ResponseEntity<Response<String>> createUser(CreateUserDTO userDTO) {
		User user = checkAuth(userDTO.getUserId());
		if (user != null)
			return ResponseEntity.status(409)
					.body(new Response<String>(false, UserError.USERID_ALREADY_EXISTS.name(), null));
		user = new User(userDTO.getUserId(), userDTO.getName(), userDTO.getBio(), new ArrayList<String>(),
				new ArrayList<String>(), userDTO.getPassword());
		userDataRepository.save(user);
		return ResponseEntity.ok().body(new Response<String>(true, UserError.SUCCESS.name(), null));
	}

	public ResponseEntity<Response<String>> addFriend(String userId, String friendId) {
		User user = checkAuth(userId);
		if (user == null)
			return ResponseEntity.status(404)
					.body(new Response<String>(false, UserError.USERID_NOT_FOUND.name(), null));

		User friend = checkAuth(friendId);
		if (friend == null)
			return ResponseEntity.status(404)
					.body(new Response<String>(false, UserError.FRIENDID_NOT_FOUND.name(), null));

		if (friend.getFriendRequests().contains(userId))
			return ResponseEntity.status(409)
					.body(new Response<String>(false, UserError.FRIEND_REQUEST_ALREADY_SENT.name(), null));

		if (user.getFriendList().contains(friendId))
			return ResponseEntity.status(409).body(new Response<String>(false, UserError.ALREADY_FRIEND.name(), null));
		friend.getFriendRequests().add(userId);
		userDataRepository.save(friend);
		return ResponseEntity.ok().body(new Response<String>(true, UserError.SUCCESS.name(), null));
	}

	public ResponseEntity<Response<String>> respondFriend(String userId, String friendId, boolean accept) {
		User user = checkAuth(userId);
		User friend = checkAuth(friendId);
		if (friend == null)
			return ResponseEntity.status(404)
					.body(new Response<String>(false, UserError.FRIENDID_NOT_FOUND.name(), null));

		if (user == null)
			return ResponseEntity.status(404)
					.body(new Response<String>(false, UserError.USERID_NOT_FOUND.name(), null));

		if (!user.getFriendRequests().contains(friendId))
			return ResponseEntity.status(404)
					.body(new Response<String>(false, UserError.NO_FRIEND_REQUEST.name(), null));

		user.setFriendRequests(
				user.getFriendRequests().stream().filter(uid -> !uid.equals(friendId)).collect(Collectors.toList()));
		if (accept) {
			user.getFriendList().add(friendId);
			friend.getFriendList().add(userId);
		}
		userDataRepository.save(user);
		userDataRepository.save(friend);
		return ResponseEntity.ok().body(new Response<String>(true, UserError.SUCCESS.name(), null));
	}

	public ResponseEntity<Response<List<UserResponse>>> findAllFriends(String userId) {
		User user = checkAuth(userId);
		if (user == null)
			return ResponseEntity.status(404)
					.body(new Response<List<UserResponse>>(false, UserError.USERID_NOT_FOUND.name(), null));
		;
		List<UserResponse> friendList = new ArrayList<UserResponse>();
		for (String friendId : user.getFriendList()) {
			User friend = checkAuth(friendId);
			if (friend != null)
				friendList.add(new UserResponse(friend.getUserId(), friend.getName(), friend.getBio(),
						friend.getFriendList()));
		}
		return ResponseEntity.ok().body(new Response<List<UserResponse>>(true, UserError.SUCCESS.name(), friendList));
	}

	public ResponseEntity<Response<String>> removeFriend(String userId, String friendId) {
		User user = checkAuth(userId);
		User friend = checkAuth(friendId);
		if (user == null)
			return ResponseEntity.status(404)
					.body(new Response<String>(false, UserError.USERID_NOT_FOUND.name(), null));
		if (friend == null || !user.getFriendList().contains(friendId))
			return ResponseEntity.status(404)
					.body(new Response<String>(false, UserError.FRIENDID_NOT_FOUND.name(), null));
		user.setFriendList(
				user.getFriendList().stream().filter(uid -> !uid.equals(friendId)).collect(Collectors.toList()));
		friend.setFriendList(
				friend.getFriendList().stream().filter(uid -> !uid.equals(userId)).collect(Collectors.toList()));
		userDataRepository.save(user);
		userDataRepository.save(friend);
		return ResponseEntity.ok().body(new Response<String>(true, UserError.SUCCESS.name(), null));
	}

}
