package com.social.distance.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.social.distance.model.User;

public interface UserDataRepository extends MongoRepository<User, String> {

}
