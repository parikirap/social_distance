package com.social.distance.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.social.distance.model.request.CreateUserDTO;
import com.social.distance.model.response.Response;
import com.social.distance.model.response.UserResponse;
import com.social.distance.service.UserDistanceService;
import com.social.distance.service.UserService;

@RestController
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private UserDistanceService userDistanceService;

	@PostMapping("/add/user")
	public ResponseEntity<Response<String>> addUser(@RequestBody CreateUserDTO userDTO) {
		return userService.createUser(userDTO);
	}

	@PostMapping("/send/friendRequest")
	public ResponseEntity<Response<String>> sendRequest(@RequestParam String userId, @RequestParam String friendId) {
		return userService.addFriend(userId, friendId);
	}

	@PostMapping("/respond/friendRequest")
	public ResponseEntity<Response<String>> respondRequest(@RequestParam String userId, @RequestParam String friendId,
			@RequestParam boolean accept) {
		return userService.respondFriend(userId, friendId, accept);
	}

	@PostMapping("/remove/friend")
	public ResponseEntity<Response<String>> removeFriend(@RequestParam String userId, @RequestParam String friendId) {
		return userService.removeFriend(userId, friendId);
	}

	@GetMapping("/list/friends")
	public ResponseEntity<Response<List<UserResponse>>> listAllFriends(@RequestParam String userId) {
		return userService.findAllFriends(userId);
	}

	@GetMapping("/get/friends/distance")
	public ResponseEntity<Response<List<UserResponse>>> findAllDistantFriends(@RequestParam String userId,
			@RequestParam int distance) {
		return userDistanceService.findPeopleAtDistance(userId, distance);
	}

}
