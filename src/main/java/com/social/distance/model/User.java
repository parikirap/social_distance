package com.social.distance.model;

import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@Document("user_data")
public class User {
	@Id
	private String userId;
	private String name;
	private String bio;
	private List<String> friendList;
	private List<String> friendRequests;
	private String password;
}
