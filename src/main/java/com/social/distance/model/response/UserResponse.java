package com.social.distance.model.response;

import java.util.List;

import lombok.AllArgsConstructor;

import lombok.Data;

@Data
@AllArgsConstructor
public class UserResponse {
	private String userId;
	private String name;
	private String bio;
	private List<String> friendList;
}
