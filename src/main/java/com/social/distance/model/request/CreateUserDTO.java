package com.social.distance.model.request;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CreateUserDTO {
	private String name;
	private String userId;
	private String bio;
	private String password;
}
