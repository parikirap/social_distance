# Social Distance #

The application is up on an EC2 instance.
### Server IP: 13.233.231.198 
### Port: 8084


### Assumptions ###
* A user will first create an account using /add/user API in order to start networking
* UserId of a person is unique and cannot be duplicated. Response message in case of duplicacy and other errors can be found in the enum UserError
* A user needs to first send request to another (valid) user inorder to network. 
* Friend will be visible once the friend request is accepted.
* Response for all the APIs is same and can be found in the Response.java class


### Database schema ###

* Database schema can be found in the User class which will be a stored as a collection in mongodb hosted at the same server.

### Using the application ###

* All the APIs can be found in the postman collection, all the APIs are working on all the edge cases that I could think of.
* Because of shortage of time could not write the JUnit tests and API description. All the API names are self explanatory incase of any issue please write to me.

### Postman collection

https://www.getpostman.com/collections/0d8246d2d56d1d30f3cd